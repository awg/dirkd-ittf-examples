ITTF Examples
=============

A few example files used for the IT Technical Forum presentation on
**Log Analysis with R** in May 2014 (https://indico.cern.ch/event/304722/)
